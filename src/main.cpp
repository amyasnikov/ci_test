
#include <iostream>
#include "math/include/math_base.h"
#include "math/include/math_vector.h"
#include "flow/include/flow_base.h"
#include "config.h"

int main() {

  std::cout << "Version " << PROJECT_VERSION_MAJOR << " " << VERSION_MINOR << std::endl;

  std::cout << "sum: " << sum(1, 3) << std::endl;
  std::cout << flow_show(flow_t{}) << std::endl;
  std::cout << vector_show(vector_t{}) << std::endl;
  std::cout << "End" << std::endl;

  return 0;
}
