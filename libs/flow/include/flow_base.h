
#include <string>

struct flow_t
{
  uint32_t saddr;
  uint32_t daddr;
  uint16_t sport;
  uint16_t dport;
};

std::string flow_show(const flow_t& flow);

