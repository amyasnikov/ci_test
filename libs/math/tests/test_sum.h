
#pragma once

TEST_CASE( "sum", "[sum]" ) {
    REQUIRE(sum(1, 2) == 3);
    REQUIRE(sum(-1, -2) == -3);
    REQUIRE(sum(2, 0) == 2);
}

TEST_CASE( "sum v2", "[sum]" ) {
    REQUIRE(sum(1, 2) == 3);
    REQUIRE(sum(-1, -2) == -3);
    REQUIRE(sum(2, 0) == 2);
}

