
add_library(math
  "src/math_base.cpp"
  "src/math_vector.cpp"
  )

target_include_directories(math PUBLIC
  "${CMAKE_CURRENT_SOURCE_DIR}/include"
  )



add_executable(math_tests "${CMAKE_CURRENT_SOURCE_DIR}/tests/tests.cpp")

target_include_directories(math_tests PUBLIC
  "${CMAKE_CURRENT_SOURCE_DIR}/include"
  "${PROJECT_SOURCE_DIR}/3rdparty"
  )

target_link_libraries(math_tests PUBLIC math)

enable_testing()
add_test(math_tests math_tests)

